interface Image {
    size: string,
    '#text': string,
}

interface Track {
    name: string,
    artist: string,
    url: string,
    streamable: string,
    listeners: number,
    image: Image[],
    mbid: string,
}