import axios from 'axios'

interface lastfmDataResponse {
    attr: any,
    'opensearch:Query': any,
    'opensearch:itemsPerPage': number,
    'opensearch:startIndex': string,
    'opensearch:totalResults': number,
    trackmatches: {
        track: Track[]
    }
}

const getTracks = async (
    trackName: string | string[] | undefined,
    page: number
): Promise<lastfmDataResponse> => {
    const response = await axios.post(
        `https://ws.audioscrobbler.com/2.0/?method=track.search&track=${trackName}&api_key=${process.env.NEXT_PUBLIC_LASTFM_API_KEY}&page=${page}&format=json`
    )

    const lastfmDataResponse: lastfmDataResponse = await response.data.results
    return lastfmDataResponse
}

export default getTracks