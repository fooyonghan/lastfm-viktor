import create from 'zustand'

type SearchStore = {
    searchTerm: string
    setSearchTerm: (searchTerm: string) => void
    page: number
    setPage: (page: number) => void
}

const useSearchStore = create<SearchStore>(
    (set): SearchStore => ({
        searchTerm: '',
        setSearchTerm: (searchTerm: string) => set(() => ({ searchTerm: searchTerm })),
        page: 1,
        setPage: (page: number) => set(() => ({ page: page }))
    })
)

export default useSearchStore