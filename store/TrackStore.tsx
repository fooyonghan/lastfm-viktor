import create from 'zustand'

type TrackStore = {
    tracks: Track[]
    setTracks: (tracks: Track[]) => void
}

const useTrackStore = create<TrackStore>(
    (set): TrackStore => ({
        tracks: [],
        setTracks: (tracks: Track[]) => set(() => ({ tracks: tracks }))
    })
)

export default useTrackStore