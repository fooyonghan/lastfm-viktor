import create from 'zustand'

type NavigationStore = {
    currentNav: number
    setNav: (currentNav: number) => void
}

const useNavigationStore = create<NavigationStore>(
    (set): NavigationStore => ({
        currentNav: 0,
        setNav: (currentNav: number) => set(() => ({ currentNav: currentNav }))
    })
)

export default useNavigationStore