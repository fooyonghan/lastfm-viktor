import { Box, Button, List, ListItem } from "@mui/material"
import { useEffect, useState } from "react"

const PlayList = () => {
    const [storedTracks, setStoredTracks] = useState<Track[]>([])

    const getLocalStorage = (key: string) => window.localStorage.getItem(key)
    const setLocalStorage = (key: string, value: any) =>
        window.localStorage.setItem(key, JSON.stringify(value))

    // Get tracks from localstorage on load
    useEffect(() => {
        const localStorageTracks = getLocalStorage('tracks')
        setStoredTracks(JSON.parse(localStorageTracks!))
    }, [])

    function removeTrack(tracks: Track[], url: string) {
        return tracks.filter(function(track) {
            return track.url !== url
        })
    }

    const handleRemoveTrack = (key: string) => {
        const localStorageTracks = getLocalStorage('tracks')
        const jsonTracks = JSON.parse(localStorageTracks!)

        const updatedList = removeTrack(jsonTracks, key)
        
        setLocalStorage('tracks', updatedList)
        setStoredTracks(updatedList)
    }

    return (
        <>
            <List>
                {storedTracks.map((storedTrack, index) => (
                    <ListItem
                        key={index}
                        disablePadding
                        sx={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            justifyItems: 'flex-start',
                            backgroundColor: '#000000',
                            color: '#ffffff',
                            padding: '8px',
                            boxShadow: 1,
                            borderRadius: 2,
                            my: 1,
                            px: 5
                        }}
                    >
                        <Box>
                            <h2><p><b>{storedTrack.name}</b></p></h2>
                            <h3><p>by <em>{storedTrack.artist}</em></p></h3>
                        </Box>
                        <Button
                            variant="contained"
                            color="secondary"
                            sx={{
                                backgroundColor: '#ff0000',
                                color: '#ffffff',
                                boxShadow: 1,
                                borderRadius: 2,
                                my: 2,
                            }}
                            onClick={() => handleRemoveTrack(storedTrack.url)}
                        >
                            Remove
                        </Button>
                    </ListItem>
                ))}
            </List>
        </>
    )
}

export default PlayList