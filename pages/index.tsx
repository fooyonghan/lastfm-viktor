import { Box, CardActionArea, CardMedia } from '@mui/material'
import type { NextPage } from 'next'

const Home: NextPage = () => {
  return (
    <>
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'row',
        }}
      >
        <Box
          sx={{
            width: '50%',
            pr: 8
          }}
        >
          <h1>Hi There!</h1>
          <p>Thanks for the nice challenge, I hope I did well. I messed up a bit  mixing up my localStorage with a Zustand store. Which I should have just thrown away in stead of fiddling with it.</p>
          <p>I added some other stuff after the deadline including a bit of styling, as at six o'clock it did not look very pretty.</p>
          <p>Thanks again and hope to see you guys soon!</p>
          <p>Cheers,</p>
          <p><em>Han</em></p>
        </Box>
        <Box
          sx={{
            width: '50%',
          }}
        >
          {/* <CardActionArea>
            <CardMedia
              component="img"
              image={'/lastfm-logo.png'}
            />
          </CardActionArea> */}
        </Box>
      </Box>
    </>
  )
}

export default Home
