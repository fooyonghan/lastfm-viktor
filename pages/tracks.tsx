import { ChangeEvent, useState } from 'react'
import { dehydrate, QueryClient, useQuery } from 'react-query'
import getTracks from '../lastfm/getTracks'
import { Container, Pagination } from '@mui/material'
import SearchResults from '../components/Search/SearchResults'
import router from 'next/router'
import useSearchStore from '../store/SearchStore'
import Search from '../components/Search/Search'
import useDebounce from '../hooks/useDebounce'

export async function getStaticProps() {
    const queryClient = new QueryClient()
    await queryClient.prefetchQuery('tracks', () => getTracks('Believe', 1))
  
    return {
        props: {
            dehydratedState: dehydrate(queryClient),
        },
    }
}

const Tracks = () => {
    const searchTerm = useSearchStore((state) => state.searchTerm)
    const debouncedSearchQuery = useDebounce(searchTerm, 600)

    const page = useSearchStore((state) => state.page)
    const setPage = useSearchStore((state) => state.setPage)

    const calculateCount = (numberOfItems: number, itemsPerPage: number): number => {
        return Math.ceil(numberOfItems / itemsPerPage)
    }

    const handlePaginationChange = async (e: ChangeEvent<unknown>, value: number) => {
        setPage(value)
        await router.push(`tracks/?page=${value}`, undefined, { shallow: true })
        refetch()
    }  

    const {
        data,
        isLoading,
        isError,
        isSuccess,
        refetch
    } = useQuery(['tracks', debouncedSearchQuery, page], () => getTracks(debouncedSearchQuery, page), {
        refetchOnWindowFocus: false,
        refetchOnMount: false,
        // enabled: false,
        keepPreviousData : true
    })

    return (
        <>
            <Search />
            { isLoading && <Container>Loading...</Container>}
            { isError && <Container>Error...</Container>}
            { isSuccess && (data!==undefined) &&
                <>
                    <Pagination
                        count={calculateCount(data?.['opensearch:totalResults']!, data?.['opensearch:itemsPerPage']!)}
                        variant='outlined'
                        color='primary'
                        className='pagination'
                        page={page}
                        onChange={handlePaginationChange}
                        sx={{
                            my: 1
                        }}
                    />
                    <SearchResults tracks={data?.trackmatches.track!} />
                </>
            }
        </>
    )
}

export default Tracks