import { Grid } from "@mui/material"
import Track from "../Track"

interface Props {
    tracks: Track[]
}

const SearchResults = ({tracks}: Props) => {
    return (
        <>
            <Grid
                container
                rowSpacing={{ xs: 1, sm: 2, md: 3 }}
                columnSpacing={{ xs: 1, sm: 2, md: 3 }}
                sx={{
                    py: 2,
                }}
            >
                {tracks.map((track, index) => (
                    <Track key={index} track={track} />
                ))}
            </Grid>
        </>
    )
}

export default SearchResults