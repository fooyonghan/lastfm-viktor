import { TextField } from "@mui/material"
import useSearchStore from "../../store/SearchStore"

const Search = () => {
    const searchTerm = useSearchStore((state) => state.searchTerm)
    const setSearchTerm = useSearchStore((state) => state.setSearchTerm)

    const handleSearchChange = (event: any) => {
        event.preventDefault();
        setSearchTerm(event.target.value)
    }
    
    return (
        <>
            <TextField
                id="outlined-basic"
                label="Search"
                variant="outlined"
                value={searchTerm}
                onChange={(e) => handleSearchChange(e)}
                sx={{
                    width: '100%'
                }}
            />
        </>
    )
}

export default Search