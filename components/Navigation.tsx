import * as React from 'react';
import Box from '@mui/material/Box';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import useNavigationStore from '../store/NavigationStore';
import router from 'next/router';

interface LinkTabProps {
    label?: string;
    href?: string;
}

function LinkTab(props: LinkTabProps) {
    return (
        <Tab
            component="a"
            onClick={(event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
                event.preventDefault();
            }}
            {...props}
        />
    );
}

export default function NavTabs() {
    const currentNav = useNavigationStore((state) => state.currentNav)
    const setCurrentNav = useNavigationStore((state) => state.setNav)

    const valueToLabel = (value: number): string => {
        let label = ''

        switch (value) {
            case 0:
                label = '/'
                break
            case 1:
                label = '/tracks'
                break
            case 2:
                label = '/playlist'
                break
            default:
                break
        }

        return label
    }

    const handleChange = async (event: React.SyntheticEvent, newValue: number) => {
        setCurrentNav(newValue)

        await router.push(`${valueToLabel(newValue)}`, undefined, { shallow: true })
    }

  return (
    <Box sx={{ width: '100%', mb: 2 }}>
      <Tabs value={currentNav} onChange={handleChange} aria-label="nav tabs example">
        <LinkTab label="Home" href="/" />
        <LinkTab label="Search" href="/tracks" />
        <LinkTab label="Playlist" href="/playlist" />
      </Tabs>
    </Box>
  );
}
