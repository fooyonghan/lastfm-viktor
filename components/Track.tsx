import { Box, Button, Container, Grid, Link } from "@mui/material"
import { useState } from "react"

interface Props {
    track: Track;
}

const Track = ({track}: Props) => {
    const [storedTracks, setStoredTracks] = useState<Track[]>([])

    const getLocalStorage = (key: string) => window.localStorage.getItem(key)
    const setLocalStorage = (key: string, value: any) =>
        window.localStorage.setItem(key, JSON.stringify(value))

    // Check if a track is currently in LocalStorage
    const trackInStore = (url: string): boolean => {
        const storedTracks: Track[] = JSON.parse(getLocalStorage('tracks')!)
        const foundTrack = storedTracks.filter(function(track) {
            return track.url === url
        })

        return foundTrack.length !== 0
    }

    function removeTrack(tracks: Track[], url: string) {
        return tracks.filter(function(track) {
            return track.url !== url
        })
    }

    const handleRemoveTrack = (key: string) => {
        const localStorageTracks = getLocalStorage('tracks')
        const jsonTracks = JSON.parse(localStorageTracks!)

        const updatedList = removeTrack(jsonTracks, key)
        
        setLocalStorage('tracks', updatedList)
        setStoredTracks(updatedList)
    }

    const handleAddTrack = (track: Track) => {
        const storedTracks = JSON.parse(getLocalStorage('tracks')!)
        storedTracks.push(track)

        setLocalStorage('tracks', storedTracks)
        setStoredTracks(storedTracks)
    }

    return (
        <>
            <Grid item xs={6} md={4}>
                <Container
                    sx={{
                        backgroundColor: '#000000',
                        color: '#ffffff',
                        padding: '8px',
                        boxShadow: 1,
                        borderRadius: 2,
                        height: '100%',
                        display: 'flex',
                        flexDirection: 'column',
                        justifyContent: 'space-between'
                    }}
                >
                    <Box>
                        <h1><p>{track.name}</p></h1>
                        <p><em>{track.artist}</em></p>
                    </Box>
                    <Box>
                        <p>
                            <Box
                                component="img"
                                alt={track.image[3]["#text"]}
                                src={track.image[3]["#text"]}
                            />
                        </p>
                        <p>
                            <Link href={track.url} variant="body2">
                                View on LastFM
                            </Link>
                        </p>
                        { 
                            !trackInStore(track.url) &&
                            <Button
                                variant="contained"
                                color="secondary"
                                sx={{
                                    backgroundColor: '#ff0000',
                                    color: '#ffffff',
                                    boxShadow: 1,
                                    borderRadius: 2,
                                    my: 2
                                }}
                                onClick={() => handleAddTrack(track)}
                            >
                                Add
                            </Button>
                        }
                        {
                            trackInStore(track.url) &&
                            <Button
                                variant="contained"
                                color="secondary"
                                sx={{
                                    backgroundColor: '#ff0000',
                                    color: '#fffff',
                                    boxShadow: 1,
                                    borderRadius: 2,
                                    my: 2,
                                }}
                                onClick={() => handleRemoveTrack(track.url)}
                            >
                                Remove
                            </Button>
                        }
                    </Box>
                </Container>
            </Grid>
        </>
    )
}

export default Track